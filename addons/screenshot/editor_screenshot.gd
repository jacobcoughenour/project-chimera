tool
extends EditorPlugin

func _input(event):
	if Input.is_key_pressed(KEY_F2):
		var viewport := get_editor_interface().get_editor_viewport().get_viewport()

		viewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
		var image := viewport.get_texture().get_data()

		# The viewport must be flipped to match the rendered window
		image.flip_y()

		viewport.set_clear_mode(Viewport.CLEAR_MODE_ALWAYS)

		# Screenshot file name with ISO 8601-like date
		var datetime := OS.get_datetime()
		for key in datetime:
			datetime[key] = str(datetime[key]).pad_zeros(2)

		var path := "editor_{year}-{month}-{day}_{hour}.{minute}.{second}.png".format(datetime)

		var error := image.save_png("user://" + path)

		if error != OK:
			push_error("Couldn't save screenshot to user directory.")
		else:
			var userpath := OS.get_user_data_dir() + "/" + path
			print("saved screenshot to " + userpath)
			OS.shell_open(userpath)

#		error = image.save_png("res://screenshots/" + path)
#
#		if error != OK:
#			push_error("Couldn't save screenshot to project.")
