extends CanvasLayer

var template = PoolStringArray([
	"[code]FPS %s  LIMIT %s  VSYNC %s",
	"POSITION x=%.3f y=%.3f z=%.3f",
	"",
	"FRAME INFO",
	"	OBJECTS             %s",
	"	VERTICES            %s",
	"	MODIFIED MATERIALS  %s",
	"	SHADER REBINDS      %s",
	"	SURFACE CHANGES     %s",
	"	DRAW CALLS          %s",
	"",
	"MEMORY",
	"	VRAM     %s / %s",
	"	TEXTURE  %s",
	"	VERTEX   %s[/code]"
]).join("\n")

export(NodePath) var render_info_label_path
var render_info_label_node
var current_camera_node

func _ready():

	render_info_label_node = get_node(render_info_label_path)
	current_camera_node = get_viewport().get_camera()


func _process(_delta):

	var position = current_camera_node.transform.origin

	render_info_label_node.bbcode_text = template % [
		Engine.get_frames_per_second(),
		"NONE" if Engine.target_fps == 0 else String(Engine.target_fps),
		"ON" if OS.vsync_enabled else "OFF",
		position.x,
		position.y,
		position.z,
		VisualServer.get_render_info(VisualServer.INFO_OBJECTS_IN_FRAME),
		VisualServer.get_render_info(VisualServer.INFO_VERTICES_IN_FRAME),
		VisualServer.get_render_info(VisualServer.INFO_MATERIAL_CHANGES_IN_FRAME),
		VisualServer.get_render_info(VisualServer.INFO_SHADER_CHANGES_IN_FRAME),
		VisualServer.get_render_info(VisualServer.INFO_SURFACE_CHANGES_IN_FRAME),
		VisualServer.get_render_info(VisualServer.INFO_DRAW_CALLS_IN_FRAME),
		VisualServer.get_render_info(VisualServer.INFO_USAGE_VIDEO_MEM_TOTAL),
		VisualServer.get_render_info(VisualServer.INFO_VIDEO_MEM_USED),
		VisualServer.get_render_info(VisualServer.INFO_TEXTURE_MEM_USED),
		VisualServer.get_render_info(VisualServer.INFO_VERTEX_MEM_USED)
	]
