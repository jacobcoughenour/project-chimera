extends Spatial

var is_paused = true

export(float) var move_speed = 16

export(NodePath) var _planet_camera_path
var _planet_camera_node

export(NodePath) var _sky_camera_path
var _sky_camera_node


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	is_paused = false

	_planet_camera_node = get_node(_planet_camera_path)
	if _sky_camera_path:
		_sky_camera_node = get_node(_sky_camera_path)


func _process(delta):

	if not is_paused:

		var move_direction = Vector3()

		if Input.is_action_pressed("move_forward"):
			move_direction += Vector3.FORWARD
		if Input.is_action_pressed("move_back"):
			move_direction += Vector3.BACK

		if Input.is_action_pressed("move_left"):
			move_direction += Vector3.LEFT
		if Input.is_action_pressed("move_right"):
			move_direction += Vector3.RIGHT
		if Input.is_action_pressed("move_up"):
			move_direction += Vector3.UP
		if Input.is_action_pressed("move_down"):
			move_direction += Vector3.DOWN

		translate(move_direction.normalized() * delta * move_speed)

	# update cameras to match

	_planet_camera_node.translation = translation
	if _sky_camera_node:
		_sky_camera_node.translation = translation / 16.0



const max_look_rotation = PI * 0.48
const look_sensitivity = 0.002

func _input(event):

	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:

		# pause game on cancel key press
		if event.is_action_pressed("ui_cancel"):
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			is_paused = true

		# capture mouse movement for look
		if event is InputEventMouseMotion:

			# relative mouse position
			var pos = (event as InputEventMouseMotion).relative

			# rotate the player
			rotation = Vector3(
				clamp(rotation.x - pos.y * look_sensitivity, -max_look_rotation, max_look_rotation),
				rotation.y - pos.x * look_sensitivity,
				rotation.z
			)

			# update camera rotations to match
			_planet_camera_node.rotation = rotation
			if _sky_camera_node:
				_sky_camera_node.rotation = rotation

	else:

		# resume game
		if event is InputEventMouseButton:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			is_paused = false

			# prevent input from bubbling to the next node
			get_tree().set_input_as_handled()

		# quit game on cancel key press
		if event.is_action_pressed("ui_cancel"):
			# exit game
			get_tree().quit()


