shader_type spatial;

render_mode depth_draw_always,specular_schlick_ggx;

float linearizeDepth(in float depth) {
	float zNear = 0.1;
	float zFar = 500.0;
	return 2.0*zFar*zNear / (zFar + zNear - (zFar - zNear)*(2.0*depth -1.0));
}

void fragment() {
	
	float depth = linearizeDepth(texture(DEPTH_TEXTURE,SCREEN_UV).r) + linearizeDepth(FRAGCOORD.z);
	
	depth *= 0.07125;
	
	ALBEDO = vec3(0.2, 0.4, 1.0);
	ALPHA = clamp(depth, 0.8, 0.98);
}