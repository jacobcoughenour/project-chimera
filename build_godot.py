#!/usr/bin/env python
import sys
import logging
import os
from threading import Thread
from os.path import dirname, join, isfile, isdir
from subprocess import Popen, PIPE, STDOUT

cwd = os.getcwd()
godot_submodule_path = join(cwd, "submodules/godot")
scon_file_path = join(godot_submodule_path, "SConstruct")
bin_path = join(godot_submodule_path, "bin")
glue_generated_path = join(
    godot_submodule_path, "modules/mono/glue/mono_glue.gen.cpp")

# https://docs.godotengine.org/en/stable/development/compiling/compiling_with_mono.html

platform = "x11"
target = "release_debug"
cores = 6

put = sys.stdout.write


def logstream(stream, logger_callback):
    while True:
        out = stream.readline()
        if out:
            logger_callback(out.rstrip())
        else:
            break


if not isfile(scon_file_path):

    put("\033[1;31m")
    print("")
    print("Godot SCons file not found at " + scon_file_path)
    put("\033[0;0m")
    print("")
    print("Maybe you forgot to initialize the submodule?")
    print("try: git submodule update --init")
    print("")

    sys.exit(1)


bin_exists = isdir(bin_path)
mono_glue_exists = isfile(glue_generated_path)

status_text = ""
status_cur = 0
status_total = 2


def run(params):
    process = Popen(
        params,
        cwd=godot_submodule_path,
        stdout=PIPE
    )

    while True:
        out = process.stdout.readline()
        if out == '' and process.poll() != None:
            break
        if out != '':
            put("\033[2K\033[1G")
            put(out)
            put("\033[0;0m[")
            put(str(status_cur))
            put("/")
            put(str(status_total))
            put("] \033[1;36m")
            put(status_text)
            put("\033[0;0m")
            sys.stdout.flush()

    return process.wait()


scons_params = [
    "scons",
    "p="+platform,
    "target="+target,
    "-j" + str(cores),
    "tools=yes",
    "module_mono_enabled=yes"
]

if not mono_glue_exists:

    put("\033[1;33m\n")
    print("Mono glue sources missing.  Regenerating...")
    put("\033[0;0m\n")

    status_text = "Building Godot tools without mono glue"
    status_cur += 1
    status_total += 1

    run(scons_params + ["mono_glue=no"])

    status_text = "Generating mono glue"
    status_cur += 1

    run([
        join(bin_path, "godot.x11.opt.tools.64.mono"),
        "--generate-mono-glue",
        "./modules/mono/glue"
    ])

status_text = "Building Godot"
status_cur = status_total

run(scons_params)
