#!/bin/bash

export EMSCRIPTEN_ROOT=[path containing em++, emsdk-portable/emscripten/*]
export ANDROID_HOME=[path to android sdk, usually /home/*/Android/Sdk]
export ANDROID_NDK_ROOT=[path to android ndk, likely $ANDROID_HOME/ndk-bundle]

X11_DEBUG=false
X11_RELEASE=false
WIN_DEBUG=false
WIN_RELEASE=false
WEB_DEBUG=false
WEB_RELEASE=false
ANDROID_DEBUG=false
ANDROID_RELEASE=false

usage () {
	echo "Usage:"
	echo "  --all builds all platforms"
	echo "  --x11 builds all linux targets"
	echo "  --win builds all windows targets"
	echo "  --web builds all html5 targets"
	echo "  --android builds all android targets"
	echo ""
	echo "  append -debug or -release for debug or release builds only"
	echo "    e.g. --all-debug"
	echo ""
	echo "This script assumes a proper environment is set up. In other words:"
	echo ""
	echo "1) All dev libs must be installed for i686 and x86-64."
	echo "2) Same for the mingw32 and mingw64 toolchains."
	echo "3) Android build tools (version 23.0.3) + NDK must be installed."
	echo "4) emscripten sdk must be installed."
	echo "5) Optional: Install upx to compress executables."
	echo ""
	echo "I'd recommend building one platform at a time on the first run to make certain you have everything you need."
}

if [[ ! $# -gt 0 ]]; then
	usage
	exit
fi

# Make sure we're in the right directory by looking for the Godot icon.
if [[ ! -f "icon.svg" ]]; then
	echo "Script must be run from root of godot repo."
	exit
fi

# Parse script arguments.
while [[ $# -gt 0 ]]; do
	key="$1"

	case $key in
		--x11-debug) X11_DEBUG=true; ;;
		--x11-release) X11_RELEASE=true; ;;
		--win-debug) WIN_DEBUG=true; ;;
		--win-release) WIN_RELEASE=true; ;;
		--web-debug) WEB_DEBUG=true; ;;
		--web-release) WEB_RELEASE=true; ;;
		--android-debug) ANDROID_DEBUG=true; ;;
		--android-release) ANDROID_RELEASE=true; ;;
		--x11) X11_DEBUG=true; X11_RELEASE=true; ;;
		--win) WIN_DEBUG=true; WIN_RELEASE=true; ;;
		--web) WEB_DEBUG=true; WEB_RELEASE=true; ;;
		--android) ANDROID_DEBUG=true; ANDROID_RELEASE=true; ;;
		--all-debug)
		X11_DEBUG=true
		WIN_DEBUG=true
		WEB_DEBUG=true
		ANDROID_DEBUG=true
		;;
		--all-release)
		X11_RELEASE=true
		WIN_RELEASE=true
		WEB_RELEASE=true
		ANDROID_RELEASE=true
		;;
		--all)
		X11_DEBUG=true
		X11_RELEASE=true
		WIN_DEBUG=true
		WIN_RELEASE=true
		WEB_DEBUG=true
		WEB_RELEASE=true
		ANDROID_DEBUG=true
		ANDROID_RELEASE=true
		;;
		*)
		usage
		exit
		;;
	esac

	shift
done

# Throw them in an array and count them up.
declare -a PLATFORMS=(\
	"$X11_DEBUG" "$X11_RELEASE"\
	"$WIN_DEBUG" "$WIN_RELEASE"\
	"$WEB_DEBUG" "$WEB_RELEASE"\
	"$ANDROID_DEBUG" "$ANDROID_RELEASE")

COUNT=0

for i in "${PLATFORMS[@]}"; do
	if [[ "$i" = true ]]; then
		COUNT=$(($COUNT + 1))
	fi
done

if [[ $COUNT -gt 0 ]]; then
	if [[ ! -d "templates" ]]; then
		mkdir templates
	fi
fi

# Linux
if [[ "$X11_DEBUG" = true || "$X11_RELEASE" = true ]]; then
	if [[ "$X11_DEBUG" = true ]]; then
		scons -j 4 p=x11 tools=no target=release_debug bits=64
		scons -j 4 p=x11 tools=no target=release_debug bits=32

		cp bin/godot.x11.opt.debug.64 templates/linux_x11_64_debug
		cp bin/godot.x11.opt.debug.32 templates/linux_x11_32_debug

		upx templates/linux_x11_64_debug
		upx templates/linux_x11_32_debug
	fi

	if [[ "$X11_RELEASE" = true ]]; then
		scons -j 4 p=x11 tools=no target=release bits=64
		scons -j 4 p=x11 tools=no target=release bits=32

		cp bin/godot.x11.opt.64 templates/linux_x11_64_release
		cp bin/godot.x11.opt.32 templates/linux_x11_32_release

		upx templates/linux_x11_64_release
		upx templates/linux_x11_32_release
	fi
fi

# Windows
if [[ "$WIN_DEBUG" = true || "$WIN_RELEASE" = true ]]; then
	if [[ "$WIN_DEBUG" = true ]]; then
		scons -j 4 p=windows tools=no target=release_debug bits=64
		scons -j 4 p=windows tools=no target=release_debug bits=32
		
		cp bin/godot.windows.opt.debug.64.exe templates/windows_64_debug.exe
		cp bin/godot.windows.opt.debug.32.exe templates/windows_32_debug.exe

		upx templates/windows_32_debug.exe
		x86_64-w64-mingw32-strip templates/windows_64_debug.exe
	fi

	if [[ "$WIN_RELEASE" = true ]]; then
		scons -j 4 p=windows tools=no target=release bits=64
		scons -j 4 p=windows tools=no target=release bits=32

		cp bin/godot.windows.opt.64.exe templates/windows_64_release.exe
		cp bin/godot.windows.opt.32.exe templates/windows_32_release.exe

		upx templates/windows_32_release.exe
		x86_64-w64-mingw32-strip templates/windows_64_release.exe
	fi
fi

# HTML5
if [[ "$WEB_DEBUG" = true || "$WEB_RELEASE" = true ]]; then
	if [[ ! -f "$EMSCRIPTEN_ROOT/em++" ]]; then
		echo "Emscripten not properly set up. Please install the Emscripten SDK.";
		exit
	fi
	
	if [[ "$WEB_DEBUG" = true ]]; then
		scons -j 4 p=javascript target=release_debug

		cp bin/godot.javascript.opt.debug.html godot.html
		cp bin/godot.javascript.opt.debug.js godot.js
		cp misc/dist/html_fs/godotfs.js .
		zip javascript_debug.zip godot.html godot.js godotfs.js
		mv javascript_debug.zip templates/
	fi

	if [[ "$WEB_RELEASE" = true ]]; then
		scons -j 4 p=javascript target=release

		cp bin/godot.javascript.opt.html godot.html
		cp bin/godot.javascript.opt.js godot.js
		cp misc/dist/html_fs/godotfs.js .
		zip javascript_release.zip godot.html godot.js godotfs.js
		mv javascript_release.zip templates/
	fi
	
	# Clean up mess.
	rm a.out.js godot.html godot.js godotfs.js
fi

# Android
if [[ "$ANDROID_DEBUG" = true || "$ANDROID_RELEASE" = true ]]; then
	if [[ ! -d "$ANDROID_HOME/build-tools/23.0.3" ]]; then
		echo "Android SDK build tools version 23.0.3 not found.";
    echo "Please install it with the Android Studio SDK manager.";
		exit
	fi
	
  # Not sure if this is necessary, t'is but a relic of the old. But it doesn't hurt.
	mkdir -p platform/android/java/libs/armeabi
	mkdir -p platform/android/java/libs/x86

	if [[ "$ANDROID_DEBUG" = true ]]; then
		scons -j 4 p=android target=release_debug
		scons -j 4 p=android target=release_debug android_arch=x86

		cd platform/android/java
		./gradlew build
		cd ../../..

		cp bin/android_debug.apk templates/android_debug.apk
	fi

	if [[ "$ANDROID_RELEASE" = true ]]; then
		scons -j 4 p=android target=release
		scons -j 4 p=android target=release android_arch=x86

		cd platform/android/java
		./gradlew build
		cd ../../..

		cp bin/android_release.apk templates/android_release.apk
	fi
fi

# Zip the template directory's contents into templates.tpz.
if [[ $COUNT -gt 0 ]]; then
	cd templates
	zip templates.tpz ./*
fi